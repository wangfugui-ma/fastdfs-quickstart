> 前提条件，linux安装fastdfs，如果没有安装的话请查看博主上一篇文章进行安装，否则无法进行下去。

## 第一步，下载源文件

下载地址：[https://codeload.github.com/happyfish100/fastdfs-client-java/zip/master](https://codeload.github.com/happyfish100/fastdfs-client-java/zip/master)
解压之后他是一个源文件，所以需要大家进行打包成一个jar包并且在自己的maven仓库去引用。
![在这里插入图片描述](https://img-blog.csdnimg.cn/123dd0fdb33b41b897f6ac25127bac7d.png)
## 第二步，maven项目中引用
打包之后我们看到版本是`1.29-SNAPSHOT`，所以我们在maven中引入：
![在这里插入图片描述](https://img-blog.csdnimg.cn/12c077d0779342f4a82141e301f01b40.png)
## 第三步，编写fastdfs.conf文件：
![在这里插入图片描述](https://img-blog.csdnimg.cn/d37ee7d54554420096f3ad0cde8a6168.png)

```java
tracker_server=127.0.0.1:22122
```
此处填写自己服务器的ip和fastdfs的端口

## 第四步，编写工具类，实现上传功能

```java
public static void upload() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);
            //上传文件到FastDFS
            //参数 1 为需要上传的文件在本地磁盘的绝对路径
            //参数 2 为需要上传的文件的扩展名
            //参数 3 为需要上传的文件的属性文件通常为null不上传，这些文件的属性例如文件大小以及类型等信息通常需要记录到数据库中
            //返回一个字符串数组，这个数组中的数据非常重要必须要妥善保管
            //注意：这个数组中的第一个元素为文件所在的FastDFS的组名，第二个元素为文件在FastDFS中的远程文件名称
            //   这两个数据通常我们是需要写入到数据库中的
            String[] result = sc.upload_file("/Users/wangfugui/Downloads/20220529151321.png", "png", null);
            for (String str : result) {
                System.out.println(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            if (sc != null) {
                try {
                    sc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

```

![在这里插入图片描述](https://img-blog.csdnimg.cn/5c77b0b8b9a84c4d82762b324e36112c.png)
## 第五步，编写下载方法

```java
public static void download() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);
            int downloadFile = sc.download_file("group1", "M00/00/00/rBJzqmKgR4OACNHEABeAcCmFn1g961.png", "/Users/wangfugui/Downloads/ss.png");
            System.out.println(downloadFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sc != null) {
                    sc.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/3d572bf2dcfc41999eeb9fd64da97296.png)
## 第六步，编写删除方法

```java
 public static void delete() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);

            int deleteFile = sc.delete_file("group1", "M00/00/00/rBJzqmKgR4OACNHEABeAcCmFn1g961.png");
            System.out.println(deleteFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sc != null) {
                    sc.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
```
![在这里插入图片描述](https://img-blog.csdnimg.cn/e7a31583242d46bda47dfba36209c36f.png)
## 第七步，编写main方法进行测试
![在这里插入图片描述](https://img-blog.csdnimg.cn/97b0821186db479e87a3173fed6781ed.png)
上传之后返回一个数组，里面的东西长这样：

```java
group1
M00/00/00/rBJzqmKgR4OACNHEABeAcCmFn1g961.png
```
我们去/00/00/文件夹里面看一下：
![在这里插入图片描述](https://img-blog.csdnimg.cn/4edfa344e14143f6bb42a78ff7070d9a.png)
删除方法，传入组名称和文件路径，就会发现服务器里面的文件删除了，
下载方法传入组名称和文件路径和本地路径，就可以发现下载成功：
![在这里插入图片描述](https://img-blog.csdnimg.cn/d3f0a40883134b509b85377d603a9d6d.png)
![在这里插入图片描述](https://img-blog.csdnimg.cn/b8e097b9d2014fbd9390f1d23e43c226.png)
仓库地址：

[https://gitee.com/WangFuGui-Ma/fastdfs-quickstart](https://gitee.com/WangFuGui-Ma/fastdfs-quickstart)