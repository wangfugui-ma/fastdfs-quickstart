package com.masiyi.fastdfs;

import org.csource.common.MyException;
import org.csource.fastdfs.ClientGlobal;
import org.csource.fastdfs.StorageClient;
import org.csource.fastdfs.StorageServer;
import org.csource.fastdfs.TrackerClient;
import org.csource.fastdfs.TrackerServer;

import java.io.IOException;

public class FastdfsUtil {

    public static void main(String[] args) {
        delete();
    }

    public static void upload() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);
            //上传文件到FastDFS
            //参数 1 为需要上传的文件在本地磁盘的绝对路径
            //参数 2 为需要上传的文件的扩展名
            //参数 3 为需要上传的文件的属性文件通常为null不上传，这些文件的属性例如文件大小以及类型等信息通常需要记录到数据库中
            //返回一个字符串数组，这个数组中的数据非常重要必须要妥善保管
            //注意：这个数组中的第一个元素为文件所在的FastDFS的组名，第二个元素为文件在FastDFS中的远程文件名称
            //   这两个数据通常我们是需要写入到数据库中的
            String[] result = sc.upload_file("/Users/wangfugui/Downloads/20220529151321.png", "png", null);
            for (String str : result) {
                System.out.println(str);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            if (sc != null) {
                try {
                    sc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void download() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);
            int downloadFile = sc.download_file("group1", "M00/00/00/rBJzqmKgR4OACNHEABeAcCmFn1g961.png", "/Users/wangfugui/Downloads/ss.png");
            System.out.println(downloadFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sc != null) {
                    sc.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void delete() {
        TrackerServer ts = null;
        StorageServer ss = null;
        StorageClient sc = null;
        try {
            //加载配置文件，目的是为了获取所有的TrackerServer的地址信息
            ClientGlobal.init("fastdfs.conf");
            TrackerClient tc = new TrackerClient();
            ts = tc.getTrackerServer();
            ss = tc.getStoreStorage(ts);
            //创建Storage的客户端对象，需要利用这个对象来操作FastDFS，实现文件的上传下载和删除
            sc = new StorageClient(ts, ss);

            int deleteFile = sc.delete_file("group1", "M00/00/00/rBJzqmKgR4OACNHEABeAcCmFn1g961.png");
            System.out.println(deleteFile);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (MyException e) {
            e.printStackTrace();
        } finally {
            try {
                if (sc != null) {
                    sc.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
